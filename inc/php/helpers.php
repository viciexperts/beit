<?PHP

/*
Omar Rodriguez
WWW.ITContinental.com

*/


function getParameter($var)

{
    $result="";
    try {
    
    if (isset($_GET[$var]))				{$result=$_GET[$var];}
        elseif (isset($_POST[$var]))		{$result=$_POST[$var];}
    
        return $result;
        
    } catch(Exception $ex) {getExceptionDetails($ex);}
}

function format_time($t,$f=':') // t = seconds, f = separator 
{
    try{
        return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
    } catch(Exception $ex) {getExceptionDetails($ex);}
}





        
 function call_api($url)
{
    try
    {
        
         $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        
        $result = curl_exec($ch);
        if ($result === false)
        {
            //echo 'Curl error: ' . curl_error($ch);
            return false;
        }
        if (curl_error($ch))
        {
            //echo curl_error($ch);
            return false;
        }
        curl_close($ch);
        $res = $result;

       return $res;

     } catch(Exception $ex) {getExceptionDetails($ex);}
}



  

function getExceptionDetails($ex)

{
    
            $code = $ex->getCode();
            $message = $ex->getMessage();
            $file = $ex->getFile();
            $line = $ex->getLine();
            return "Exception thrown in $file on line $line: [Code $code]
            $message";
            
            
}

function fetch_array_from_query($query,$link){
    
	try {
	
            $responseArray=[];
            
             $result = mysql_to_mysqli($query, $link);

                while($row = $result->fetch_array())
                    {
                    $responseArray[] = $row;
                    }
                    
                        return $responseArray;
			
			
		 } catch(Exception $ex) {getExceptionDetails($ex);}
}

function build_table($array,$attributes="",$headersOnly=0){
    
	try {
	
			// start table
			$html = "<table $attributes >";
			// header row
			$html .= '<thead>';
			
                        $headerRow .= '<tr>';
			foreach($array[0] as $key=>$value){
					$headerRow .= '<th>' . htmlspecialchars($key) . '</th>';
				}
			$headerRow .= '</tr>';
                        
                        
			$html .= $headerRow;
			$html .= '</thead>';
			
                        if($headersOnly<>1){    
                            $html .= '<tbody>';

                            // data rows
                            foreach( $array as $key=>$value){
                                    $html .= '<tr>';
                                    foreach($value as $key2=>$value2){
                                            $html .= '<td>' . htmlspecialchars($value2) . '</td>';
                                    }
                                    $html .= '</tr>';
                            }

                            // finish table and return it

                            $html .= '</tbody>';
                        }
                        
                        $html .= '<tfoot>';
                        
                        $html .= $headerRow;
                        
			$html .= '</tfoot>';
                        
                        
                        
			$html .= '</table>';
			return $html;
			
			
		 } catch(Exception $ex) {getExceptionDetails($ex);}
}
function explode_implode_to_SQL($text,$separator=" "){
    
	try {
	
			 
                $string_array = explode($separator, $text);
                $string_sql = "'" . implode( "','", $string_array) . "'";
                
                
			return $string_sql;
			
			
		 } catch(Exception $ex) {getExceptionDetails($ex);}
}



function GetData($DBLink, $Query) {
    if (DEBUG) {
        echo PRE . "Query: $Query\n" . PRE_END;
    }
    if ($Result = $DBLink->query($Query)) {
        if (DEBUG) {
            printf(PRE . "Affected rows (Non-Select): %d\n" . PRE_END, $DBLink->affected_rows);
        }
        while ($Record = $Result->fetch_assoc()) {
            $ReturnData[] = $Record;
			
        }
		
		//if (DEBUG) {   print_r($ReturnData);   }
		
        return $ReturnData;
    } else {
        if (DEBUG) {
            printf(PRE . "Errormessage: %s\n", $DBLink->error);
            printf("Affected rows (Non-Select): %d\n", $DBLink->affected_rows);
            echo "No Records Returned\n" . PRE_END;
        }
        return false;
    }
}
function UpdateData($DBLink, $Query) {
    if (DEBUG) {
        echo PRE . "Query: $Query\n" . PRE_END;
    }
    if ($Result = $DBLink->query($Query)) {
        if (DEBUG) {
            printf(PRE . "%s\n", $DBLink->info);
            printf("Affected rows (Non-Select): %d\n" . PRE_END, $DBLink->affected_rows);
        }
       // debug_to_console($DBLink->info);
       // debug_to_console($DBLink->affected_rows);
        
        return;
    } else {
        if (DEBUG) {
            printf(PRE . "Errormessage: %s\n", $DBLink->error);
            printf("Affected rows (Non-Select): %d\n", $DBLink->affected_rows);
            echo "No Records Returned\n" . PRE_END;
        }
        return;
    }
}

function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);
	
	$output=addslashes($output);
	$output = str_replace(array("\r\n", "\n", "\r"), ' ', $output);
	
    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}




function array_filter_by_value($my_array, $index, $value)
    
    { 
      try{
        if(is_array($my_array) && count($my_array)>0)  
        { 
            foreach(array_keys($my_array) as $key){ 
                $temp[$key] = $my_array[$key][$index]; 
                 
                if ($temp[$key] == $value){ 
                    $new_array[$key] = $my_array[$key]; 
                } 
            } 
          } 
        return $new_array; 
       } catch(Exception $ex) {getExceptionDetails($ex);}
    } 



?>