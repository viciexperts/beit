<?php


/**
 * 
  * WWW.ITContinental.com
 * @author    Omar Rodriguez <Omar@ITContinental.com>
 * 
 */


$domain = $_SERVER['HTTP_HOST'];
$prefix = array_key_exists('HTTPS', $_SERVER) && $_SERVER["HTTPS"] ? 'https://' : 'http://';

define('URL_VICIDIAL_NON_AGENT_API', "$prefix$domain/vicidial/non_agent_api.php");
define('URL_VICIDIAL_AGENT_API', "$prefix$domain/agc/api.php");
define('MAIN_QUERY', "SELECT Campaign_Name,
                                        Calls,
                                        ifnull(OutDrop, 0) + ifnull(INDROP, 0) AS Lost,
                                        talk / Calls AS AVGCalls,
                                        queue_seconds / INDROP AS AVGWait
                                 FROM (SELECT campaign_id, sum(if(lead_id IS NOT NULL, 1, 0)) AS Calls
                                       FROM vicidial_agent_log
                                       WHERE event_time BETWEEN 'date1'
                                                            AND concat('date2', ' 23:59')
                                       GROUP BY 1) AS a
                                      LEFT JOIN
                                      (SELECT campaign_id, sum(if(status = 'DROP', 1, 0)) AS OutDrop
                                       FROM vicidial_log
                                       WHERE call_date BETWEEN 'date1' AND concat('date2', ' 23:59')
                                       GROUP BY 1) AS o
                                         ON a.campaign_id = o.campaign_id
                                      LEFT JOIN
                                      (SELECT f_getCampaign(campaign_id) campaign_id,
                                                sum(queue_seconds) AS queue_seconds,
                                              count(0) as INDROP
                                       FROM vicidial_closer_log
                                      
                                       WHERE call_date BETWEEN 'date1' AND concat('date2', ' 23:59')
                                              and status = 'DROP' 
                                       GROUP BY 1) AS i
                                         ON a.campaign_id = i.campaign_id
                                      JOIN vicidial_campaigns c ON c.campaign_id = a.campaign_id
                                      LEFT JOIN
                                      (SELECT campaign_id,
                                              sum(
                                                 if(
                                                        pause_sec < 36000
                                                    AND wait_sec < 36000
                                                    AND talk_sec < 36000
                                                    AND dispo_sec < 36000
                                                    AND lead_id IS NOT NULL,
                                                    talk_sec,
                                                    0))
                                                 talk,
                                              sum(
                                                 if(
                                                        pause_sec < 36000
                                                    AND wait_sec < 36000
                                                    AND talk_sec < 36000
                                                    AND dispo_sec < 36000
                                                    AND lead_id IS NOT NULL,
                                                    wait_sec,
                                                    0))
                                                 wait
                                       FROM vicidial_agent_log
                                       WHERE event_time BETWEEN 'date1'
                                                            AND concat('date2', ' 23:59')
                                       GROUP BY 1) AS b
                                         ON b.campaign_id = a.campaign_id");
define('AGENT_QUERY', "SELECT full_name, sum(if(lead_id IS NOT NULL, 1, 0)) AS Calls
                        FROM vicidial_agent_log JOIN vicidial_users USING (user)
                        WHERE event_time BETWEEN 'date1' AND concat('date2', ' 23:59')
                        GROUP BY 1");

define('REALTIME_QUERY1', "SELECT Campaign_name Campaign,
                                            ifnull(Ready,0) Ready,
                                            ifnull(INCALL,0) INCALL,
                                            ifnull(Live, 0) Live,
                                            time_to_sec(timediff(now(),oldest_call_time)) oldest_call_time
                                     FROM vicidial_campaigns
                                          LEFT JOIN
                                          (SELECT campaign_id,
                                                  sum(if(status IN ('READY', 'CLOSER'), 1, 0)) AS Ready,
                                                  sum(if(status = 'INCALL', 1, 0)) AS INCALL
                                           FROM vicidial_live_agents
                                           WHERE on_hook_agent = 'N'
                                           GROUP BY 1) AS a
                                             USING (campaign_id)
                                          LEFT JOIN
                                          (SELECT CASE
                                                     WHEN call_type = 'IN' THEN f_getCampaign(campaign_id)
                                                     ELSE campaign_id
                                                  END
                                                     AS campaign_id,
                                                  count(0) Live,
                                                  min(call_time) oldest_call_time
                                           FROM vicidial_auto_calls
                                           WHERE status NOT IN ('XFER')
                                           GROUP BY 1) AS b
                                             USING (campaign_id)");
define('REALTIME_QUERY2', "SELECT campaign_name,
                                full_name,
                                case
                                 when v.lead_id is null and a.status in ('INCALL')then 'DEAD'
                                 when a.lead_id > 0 and a.status in ('READY','PAUSED')then 'DISPO'
                                 else a.status
                                 end as  status,
                                CASE
                                   WHEN a.status IN ('INCALL',
                                                     'DIAL',
                                                     'QUEUE',
                                                     'PARK',
                                                     '3-WAY')
                                   THEN
                                      UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(last_call_time)
                                   ELSE
                                      UNIX_TIMESTAMP(now()) - UNIX_TIMESTAMP(last_state_change)
                                END
                                   AS `agent_status_time`,
                                last_call,
                                talk / calls
                                   AS `average_call_time`,
                                   calls
                         FROM vicidial_live_agents AS a
                              JOIN vicidial_users AS u USING (user)
                              LEFT JOIN
                              (SELECT campaign_id,
                                      user,
                                      sum(if(lead_id IS NOT NULL, 1, 0))
                                         AS Calls,
                                      sum(
                                         if(
                                                pause_sec < 36000
                                            AND wait_sec < 36000
                                            AND talk_sec < 36000
                                            AND dispo_sec < 36000
                                            AND lead_id IS NOT NULL,
                                            talk_sec,
                                            0))
                                         talk
                               FROM vicidial_agent_log
                               WHERE event_time > curdate()
                               GROUP BY 1, 2) AS b
                                 ON a.user = b.user AND a.campaign_id = b.campaign_id
                              JOIN vicidial_campaigns c ON a.campaign_id = c.campaign_id
                              LEFT JOIN (SELECT a.campaign_id, a.user, talk_sec AS last_call
                                         FROM vicidial_agent_log a
                                              JOIN
                                              (SELECT campaign_id, user, max(agent_log_id) id
                                               FROM vicidial_agent_log
                                               WHERE     event_time > curdate()
                                                     AND lead_id IS NOT NULL
                                                     AND lead_id NOT IN
                                                            (SELECT lead_id FROM vicidial_live_agents)
                                               GROUP BY 1, 2) b
                                                 ON     a.campaign_id = b.campaign_id
                                                    AND a.user = b.user
                                                    AND a.agent_log_id = b.id) AS s
                                 ON s.campaign_id = a.campaign_id AND s.user = a.user
                                 left join vicidial_auto_calls v on v.lead_id = a.lead_id
                                 order by 1,3,2 ");

### CUSTOMIZE TIMES FOR REALTIME DISPLAY ###
$rt_report_times["waiting_short_time"]=30;
$rt_report_times["waiting_medium_time"]=60;
$rt_report_times["waiting_long_time"]=300;
$rt_report_times["incall_short_time"]=10;
$rt_report_times["incall_medium_time"]=60;
$rt_report_times["incall_long_time"]=300;
$rt_report_times["paused_short_time"]=10;
$rt_report_times["paused_medium_time"]=60;
$rt_report_times["paused_long_time"]=300;
$rt_report_times["threeway_short_time"]=10;
$rt_report_times["dead_short_time"]=10;
$rt_report_times["pause_limit"]=20;






?>


