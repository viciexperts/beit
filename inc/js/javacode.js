/* 
 * Omar Rodriguez
 * WWW.ITContinental.com
 
 */

$(window).ready( function() {
    
    var time = 4
   
    setInterval( function() {
        
        time--;
        
        $('#time').html(time);
        
        if (time === 0) {
            
            location.reload()
        }    
        
        
    }, 1000 );
    
});

function getQueryString(key) {
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

function ShowToast(message) {
      var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        
        $('#snackbar').text(message);
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
    
}


