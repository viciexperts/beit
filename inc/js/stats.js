/* 
 * Omar Rodriguez
 * WWW.ITContinental.com
 
 */
var myInterval;


$( "#btn_run" ).click(function() {
  

  
    if ( (document.getElementById("date_range_picker").value=="") || (document.getElementById("date_range_picker").value==null)   )	
		
		{
			alert("You must select a date before continue");
			
			 return;
		}
            var json_date = JSON.parse(  document.getElementById("date_range_picker").value   );
	
  
  
		fx_get_report_data(json_date);

    
});

var waitingText=[

               function() { $('body').loadingModal('text', 'Ensuring Everything Works Perfectly').loadingModal('animation', 'rotatingPlane').loadingModal('backgroundColor', 'red')},
               function() { $('body').loadingModal('text', 'Doing The Impossible').loadingModal('animation', 'wave')},
               function() { $('body').loadingModal('text', 'Generating Plans for Faster-Than-Light Travel').loadingModal('animation', 'wanderingCubes').loadingModal('backgroundColor', 'green')},
               function() { $('body').loadingModal('text', 'Oiling Clockworks').loadingModal('animation', 'spinner')},
               function() { $('body').loadingModal('text', 'Creating Randomly Generated Feature').loadingModal('animation', 'chasingDots').loadingModal('backgroundColor', 'blue')},
               function() { $('body').loadingModal('text', 'Polishing Erudite Foreheads').loadingModal('animation', 'threeBounce')},
               function() { $('body').loadingModal('text', 'Checking Anti-Camp Radius').loadingModal('animation', 'circle').loadingModal('backgroundColor', 'black')},
               function() { $('body').loadingModal('text', 'Doing Something You Don\'t Wanna Know About').loadingModal('animation', 'cubeGrid')},
               function() { $('body').loadingModal('text', 'Adding Vanilla Flavor to Ice Giants').loadingModal('animation', 'fadingCircle').loadingModal('backgroundColor', 'gray')},
               function() { $('body').loadingModal('text', 'Ensuring Gnomes Are Still Short').loadingModal('animation', 'foldingCube')}

     ];
  


     $(document).on({
     ajaxStart: function() {
         
      showModal();  
         //
      myInterval = setInterval(showModal, 2000);    
     },
     ajaxStop: function() { hideModal();  }    
});


$(document).ready(function(){
  
		

                $('#wait').hide();

		$("#date_range_picker").daterangepicker({
			datepickerOptions : {
				 numberOfMonths : 2
			 }
		 });
	
  });



function getQueryString(key) {
    key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
    var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
    return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

function ShowToast(message) {
      var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        
        $('#snackbar').text(message);
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
    
}



function fx_get_report_data( date_range){
    
    try {

	$.ajax({
		
        type: "GET",
        url: "inc/php/data.php",
		data: {
				'date1':date_range.start,
				'date2':date_range.end
				
				},
		
		
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        
		
		
		success: function (response) {
         
		 
                      var json_response= response;
             
             
                  $("#report_div").show();
		
		
               
                
                var divContainer = document.getElementById("report_area");
    if ( (json_response=="") || (json_response==null) || (json_response.data.length==0) )	
            {
                    

                      divContainer.innerHTML = ' <div align="center" class="alert alert-danger" role="alert">  There is no data to display for the selected filters</div>';
            }

		 
	else{
  
                var HTMLtable = '<table id="report_table" class="table table-hover  table-bordered   align-middle ">';
                                
                       HTMLtable +=  " <thead><tr><th>Description</th><th>Total</th></tr></thead> ";         
                       HTMLtable +=  " <tbody> ";         
                                
	
		 
                   
                    for (var i = 0; i < json_response.data.length; i++) {
                        
               
                          HTMLtable +=  " <tr> <td class='expand'>" + "Total Call Number for "+json_response.data[i].Campaign_Name + "</td><td class='expand'>" + json_response.data[i].Calls + "</td></tr>";   
                        
                    }
                    for (var i = 0; i < json_response.data.length; i++) {
                        
                   
                          HTMLtable +=  " <tr> <td class='expand'>" + "Total Call Lost for "+json_response.data[i].Campaign_Name + "</td><td class='expand'>" + json_response.data[i].Lost + "</td></tr>";   
                        
                    }
                    for (var i = 0; i < json_response.agent.length; i++) {
                        
                   
                          HTMLtable +=  " <tr> <td class='expand'>" + "Total Calls for "+json_response.agent[i].full_name + "</td><td class='expand'>" + json_response.agent[i].Calls + "</td></tr>";   
                        
                    }
       
                 
                    for (var i = 0; i < json_response.data.length; i++) {
                        
                         formatted = moment.utc(json_response.data[i].AVGCalls*1000).format('mm:ss');
                          HTMLtable +=  " <tr> <td class='expand'>" + "Average time for a call in  "+json_response.data[i].Campaign_Name + "</td><td class='expand'>" + formatted + "</td></tr>";   
                        
                    }
                    for (var i = 0; i < json_response.data.length; i++) {
                        
                            formatted = moment.utc(json_response.data[i].AVGWait*1000).format('mm:ss');
                          HTMLtable +=  " <tr> <td class='expand'>" + "Average time for a call wating in  "+json_response.data[i].Campaign_Name + "</td><td class='expand'>" + formatted + "</td></tr>";   
                        
                    }
                            HTMLtable +=  " </tbody> </table>";         

                             divContainer.innerHTML=HTMLtable;
                             
                             
                               $('#report_table').DataTable( {
                                   "bPaginate": false,
                                    fixedHeader: true,
                                      dom: 'Bfrti',
                                    buttons: [
                                            
                                                { extend: 'excelHtml5', footer: true,title: 'Stats' },
                                            
                                            ]
                                  //  "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ]        ,
                                  //  "paging":   true,
                                    /*
                                    "ordering": false,
                                    "info":     true,
                                    "autoWidth":true,
                                     select: true,
                                    // "scrollY": true,
                                    fixedHeader: true,*/
                                  //  lengthChange : true,
                                 /*   dom: 'Bfrti',
                                    buttons: [
                                            
                                                { extend: 'excelHtml5', footer: true,title: 'Stats' },
                                            
                                            ]
                                            
                                            */

                                } );
                             
     
            
		}	  
		 
		
		
	
	
	

	hideModal();	
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("statusText:" + xhr.statusText);
            console.log("responseText:" + xhr.responseText);
            console.log("status:" + xhr.status);
			
                        hideModal();
          //  console.log("thrownError:" + thrownError);
        }
    });


    }
    catch(err) {
      console.log(err.message);
      hideModal();
      
    }

}


function showModal() {
        
    
        $('body').loadingModal({text: 'Please wait...'});
        waitingText[ Math.floor(Math.random() * waitingText.length) ]();
        
        
    }

function hideModal(){
$('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)').loadingModal('backgroundColor', 'yellow'); 
	
        
 setTimeout(function() {
    $('body').loadingModal('hide'); 
   $('body').loadingModal('destroy');

    clearInterval(myInterval);
}, 500);


}

function validURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return !!pattern.test(str);
}





//var htmlAtributes = ["title","style","class"];
var htmlAtributes = [
    'accept',
'accept-charset',
'accesskey',
'action',
'align',
'allow',
'alt',
'async',
'autocapitalize',
'autocomplete',
'autofocus',
'autoplay',
'background',
'bgcolor',
'border',
'buffered',
'capture',
'challenge',
'charset',
'checked',
'cite',
'class',
'code',
'codebase',
'color',
'cols',
'colspan',
'content',
'contenteditable',
'contextmenu',
'controls',
'coords',
'crossorigin',
'csp ',
'data',
'datetime',
'decoding',
'default',
'defer',
'dir',
'dirname',
'disabled',
'download',
'draggable',
'enctype',
'enterkeyhint ',
'for',
'form',
'formaction',
'formenctype',
'formmethod',
'formnovalidate',
'formtarget',
'headers',
'height',
'hidden',
'high',
'href',
'hreflang',
'http-equiv',
'icon',
'id',
'importance ',
'inputmode',
'integrity',
'intrinsicsize ',
'ismap',
'itemprop',
'keytype',
'kind',
'label',
'lang',
'language',
'list',
'loading ',
'loop',
'low',
'manifest',
'max',
'maxlength',
'media',
'method',
'min',
'minlength',
'multiple',
'muted',
'name',
'novalidate',
'open',
'optimum',
'pattern',
'ping',
'placeholder',
'poster',
'preload',
'radiogroup',
'readonly',
'referrerpolicy',
'rel',
'required',
'reversed',
'rows',
'rowspan',
'sandbox',
'scope',
'scoped',
'selected',
'shape',
'size',
'sizes',
'slot',
'span',
'spellcheck',
'src',
'srcdoc',
'srclang',
'srcset',
'start',
'step',
'style',
'summary',
'tabindex',
'target',
'title',
'translate',
'type',
'usemap',
'value',
'width',
'wrap'
      
];


function validateHhMm(value) {
    
   return  /^([0-9]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(value);
    /*var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

    if (isValid) {
      inputField.style.backgroundColor = '#bfa';
    } else {
      inputField.style.backgroundColor = '#fba';
    }

    return isValid;*/
  }
  
  
  function truncateText() {
    var showChar = 40;
    var ellipsestext = "...";

    $(".truncate").each(function() {
      var content = $(this).html();
      if (content.length > showChar) {
        var c = content.substr(0, showChar);
        var h = content;
        var html =
          '<div class="truncate-text" style="display:block">' +
          c +
          '<span class="moreellipses">' +
          ellipsestext +
          '&nbsp;&nbsp;<a href="" class="moreless more">more</a></span></span></div><div class="truncate-text" style="display:none">' +
          h +
          '<a href="" class="moreless less">Less</a></span></div>';

        $(this).html(html);
      }
    });

    $(".moreless").click(function() {
      var thisEl = $(this);
      var cT = thisEl.closest(".truncate-text");
      var tX = ".truncate-text";

      if (thisEl.hasClass("less")) {
        cT.prev(tX).toggle();
        cT.slideToggle();
      } else {
        cT.toggle();
        cT.next(tX).fadeToggle();
      }
      return false;
    });
    /* end iffe */
  }