<!DOCTYPE html>
<!--
Omar Rodriguez
WWW.ITContinental.com

-->
<html lang="en">
<head>
  <title>Realtime</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
  <script src="inc/js/javacode.js?filever=<?=filesize('inc/js/javacode.js')?>" crossorigin="anonymous"  ></script>
  
  <link rel="stylesheet" type="text/css" href="inc/css/style.css"/>

</head>
<body>

 <?php
        
        require("../dbconnect_mysqli.php");
        require("../functions.php");
       
        require "inc/php/constants.php";
        require  "inc/php/helpers.php";
        
        $STARTtime = date("U");
         


?>
    <div class="clearfix">
        
        
<div class="container-fluid full-height ">
  
       
 <?php

        $campaign_array=fetch_array_from_query(REALTIME_QUERY1,$link);
 
           
        $agent_array=fetch_array_from_query(REALTIME_QUERY2,$link);
        
      
        
        $i=0;
        foreach($campaign_array as $item) {
            
            $Campaign=$item['Campaign'];
            
            echo "<h1>$Campaign"; 
            
            if ($i==0) echo "<small> -- Reloading in: <span id='time'>4</span></small>";
            echo "</h1>"; 
            
            $i++;
            
            ?>
    
              <table class="table table-hover  table-bordered table-sm" cellpadding="0" cellspacing="0" >

                <thead>
                  <tr>
                    <th  class="expand">Calls Waiting for Agents</th>
                    <th  class="expand">First Call Waiting Time</th>
                    <th  class="expand">Agents Ready</th>
                    <th  class="expand">Agents in call</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td  class="expand"> <?php echo $item['Live']; ?></td>
                    <td  class="expand"> <?php echo sprintf("%7s", sec_convert($item['oldest_call_time'],'M')); ?></td>
                    <td  class="expand"> <?php echo $item['Ready']; ?></td>
                     <td  class="expand"> <?php echo $item['INCALL']; ?></td>
                    
                  </tr>

                </tbody>
              </table>
          
        
     <?php 
     
     $agent_array_filtered = array_filter_by_value($agent_array, 'campaign_name', $Campaign); 
     if(is_array($agent_array_filtered) && count($agent_array_filtered)>0){  ?>
           <table class="table table-hover table-bordered table-sm" cellpadding="0" cellspacing="0" >

                <thead>
                  <tr>
                    <th>Agent Name</th>
                    <th>Agent status</th>
                    <th>Agent status time</th>
                   <!-- <th>Last call time</th>
                    <th>Average call time</th> -->
                    <th>Calls</th>
                  </tr>
                </thead>
                <tbody>
                  
   
                  
         
            <?
            
            
           
                 
                    foreach($agent_array_filtered as $item) {
                        $tr_class='TRblank';
                        
                            if ( ($item['status']=='INCALL') or ($item['status']=='PARK') )
                                    {
                                            {
                                            if ($item['agent_status_time'] >= $rt_report_times["incall_short_time"]) { $tr_class='TRthistle';}
                                            if ($item['agent_status_time'] >= $rt_report_times["incall_medium_time"]) { $tr_class='TRviolet';}
                                            if ($item['agent_status_time'] >= $rt_report_times["incall_long_time"]) { $tr_class='TRpurple';}

                                            }
                                    }
                            if ($item['status']=='3-WAY')
                                    {
                                    if ($item['agent_status_time'] >= $rt_report_times["threeway_short_time"]) { $tr_class='TRlime';}
                                    }
                            if ($item['status']=='DEAD')
                                    {

                                            if ($item['agent_status_time'] >= $rt_report_times["dead_short_time"]) {$tr_class='TRblack';}

                                    }
                            if ($item['status']=='DISPO')
                                    {
                                    $tr_class='TRblank';
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_short_time"]) { $tr_class='TRkhaki';}
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_medium_time"]) {$tr_class='TRyellow';}
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_long_time"]) { $tr_class='TRolive';}
                                            

                                    }
                            if ($item['status']=='PAUSED') 
                                    {
                                     $tr_class='TRblank';
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_short_time"]) { $tr_class='TRkhaki';}
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_medium_time"]) {$tr_class='TRyellow';}
                                            if ($item['agent_status_time'] >= $rt_report_times["paused_long_time"]) { $tr_class='TRolive';}
                                        

                                    }





                            if ( (preg_match("/READY/i",$item['status'])) or (preg_match("/CLOSER/i",$item['status'])) ) 
                                    {

                                             $tr_class='TRlightblue';
                                            if ($item['agent_status_time'] >= $rt_report_times["waiting_medium_time"]) {$tr_class='TRblue';}
                                            if ($item['agent_status_time'] >= $rt_report_times["waiting_long_time"]) { $tr_class='TRmidnightblue';}

                                    }

                            if ($Astatus[$i] == 'RING')
                                    {

                                     $tr_class='TRblank';
                                      if ($item['agent_status_time'] >= 0) {$tr_class='TRsalmon';}
                                    }


                            echo "<tr class='$tr_class ". substr($tr_class, 2) ." '>";
                            echo "<td class='expand'>".$item['full_name']."</td>";
                            echo "<td class='expand' >".$item['status']."</td>";
                            
                            echo "<td class='expand'>".sprintf("%7s", sec_convert($item['agent_status_time'],'M')) ."</td>";
                            /*echo "<td class='expand'>".sprintf("%7s", sec_convert($item['last_call'],'M')) ."</td>";
                            echo "<td class='expand'>".sprintf("%7s", sec_convert($item['average_call_time'],'M')) ."</td>"; */
                             echo "<td class='expand' >".$item['calls']."</td>";
                            echo "</tr>";
                    }
             
                    ?>
            

                </tbody>
              </table>
       
            
        <?}
        
        }
        
        if (count($campaign_array)==0){?>
          
         <table class="table table-hover  table-bordered table-sm" cellpadding="0" cellspacing="0" >

                <thead>
                  <tr>
                    <th  class="expand">Calls Waiting for Agents</th>
                    <th  class="expand">Agents Ready</th>
                    <th  class="expand">Agents in call</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td  class="expand"> 0</td>
                    <td  class="expand"> 0</td>
                    <td  class="expand"> 0</td>
                    
                  </tr>

                </tbody>
              </table>
    
        <?}
            
            
            
       
 
 ?>

</div>
</div>

</body>
</html>
