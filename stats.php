<!DOCTYPE html>
<!--
Omar Rodriguez
WWW.ITContinental.com

-->

 <?php
	
header('Set-Cookie: cross-site-cookie=name; SameSite=None; Secure');	
setcookie('key', 'value', time()+(7*24*3600), "/; SameSite=None; Secure");


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(1);

        require("../dbconnect_mysqli.php");
        require("../functions.php");
       
        require "inc/php/constants.php";
        require  "inc/php/helpers.php";
        
        $STARTtime = date("U");

$user = '';
/*require("authentication.php");

$user=$PHP_AUTH_USER;
*/

$report_name='Custom Reports';



?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="author" content="viciexperts.com">
    <title>Custom Reports</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet" crossorigin="anonymous"/>
   
	
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-loadingModal@1.0.2/css/jquery.loadingModal.css" crossorigin="anonymous"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"  />
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap/3.1.1/css/bootstrap-theme.min.css">
   
	
    <link rel="stylesheet" href="inc/css/jquery.comiseo.daterangepicker.css">

       <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/cr-1.5.3/fc-3.3.2/fh-3.1.8/r-2.2.7/rr-1.2.7/sc-2.0.3/sl-1.3.3/datatables.min.css"/>
        
     




    <link href="inc/css/stats.css" rel="stylesheet" />
  </head>
  
  <body>
    <div class="s002">
      <form>
        <fieldset>
          <legend>Custom Reports</legend>
        </fieldset>
        <div class="inner-form">
          
          
		  
		 
        
	 <div class="input-field first-wrap input-group date" style="margin-left: 30px;">
            
							
		<input id="date_range_picker" type="text" class="form-control"  />
							   
		
            
          </div>
		  
		 
		  
          <div class="input-field fifth-wrap">
            <button id="btn_run" name="btn_run" class="btn-search" type="button">SEARCH</button>
          </div>
        </div>
      </form>
	  
	  
	  
	  
    </div>
   
   
   
   <!-- Report Area-->
   
   <div class="dashbord" id="report_div" >
		<div class="container">

			<div id="report_area" name="report_area" class="report_area" style="padding-top: 0px;">
			
			
			</div>

		
        
		</div>
	</div>
	
	


	
	

	
	
	
	
	
	<input type="hidden" id="user" name="user" <?php echo 'value="' . $PHP_AUTH_USER . '"';  ?> ></input>
   
                        <!--
                        <pre>
			<?
				echo $HEADER_b;
			
			?>
			<pre>
			-->
   
   
   <!-- template scripts -->
    <script src="inc/js/extention/choices.js" 	></script>




<!--Jquery-->


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"  ></script> -->
 
<!-- Data Tables
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/fc-3.3.2/fh-3.1.8/r-2.2.7/sc-2.0.3/sl-1.3.3/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/b-print-1.7.0/fc-3.3.2/fh-3.1.8/r-2.2.7/sc-2.0.3/sl-1.3.3/datatables.min.js"></script>
 -->
<!-- Data Tables  -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.24/b-1.7.0/b-colvis-1.7.0/b-html5-1.7.0/cr-1.5.3/fc-3.3.2/fh-3.1.8/r-2.2.7/rr-1.2.7/sc-2.0.3/sl-1.3.3/datatables.min.js"></script>
     
 
        
        
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js "  crossorigin="anonymous" defer ></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment-with-locales.min.js" defer ></script>


    
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js" defer ></script> 
    
 <script src="inc/js/jquery.comiseo.daterangepicker.js" defer ></script>

<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"  crossorigin="anonymous" ></script> 










<script src="https://cdn.jsdelivr.net/npm/jquery-loadingModal@1.0.2/js/jquery.loadingModal.js" crossorigin="anonymous"></script>



 
    
	
<script src="inc/js/stats.js?filever=<?=filesize('./js/stats.js')?>" crossorigin="anonymous"  ></script>




<?
	// echo "<pre>status_stmt:$status_stmt</pre>";
	
	?>

  </body>
</html>
